(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});

//SendAnywhere API

  $(document).ready(function () {
	function updateDevice() {
		$.ajax({
			url: 'https://send-anywhere.com/web/v1/device',
			type: 'GET',
			dataType: 'jsonp',
			data: {api_key: "1d2d129ee48904b9b8e072737059032c8558af16", profile_name: "tekamolowebapis"},
			cache: false
		}).done(function (data) {
		});
	}
	function createKey(file) {
		$.ajax({
			url: 'https://send-anywhere.com/web/v1/key',
			type: 'GET',
			dataType: 'jsonp',
			cache: false
		}).done(function (data, textStatus, xhr) {
			$('#key').text(data.key);
			var formData = new FormData($('#sendForm')[0]);
			sendFile(data.weblink, formData);
		})
	}
	function sendFile(url, data) {
		$.ajax({
			url: url,
			type: 'POST',
			processData: false,
			contentType: false,
			data: data,
			cache: false
		}).done(function (data) {
		});
	}
	function receiveKey(key) {
		$('#status').text('waiting');
		$.ajax({
			url: 'https://send-anywhere.com/web/v1/key/' + key,
			type: 'GET',
			dataType: 'jsonp',
			timeout: 10000,
			cache: false
		}).done(function (data) {
			receiveFile(data.weblink);
			$('#status').text('done');
		}).fail(function (xhr, textStatus, error) {
			$('#receiveForm .form-group').addClass('has-error');
			$('#status').text('failed').removeClass('text-success').addClass('text-danger');
		});
	}
	function receiveFile(url) {
		$('<iframe />')
			.attr('src', url)
			.hide()
			.appendTo('body');
	}
	$('#sendBtn').click(function (){
		var files = $('#fileInput').prop('files')
		if (files.length > 0) {
			createKey(files[0]);
		}
	});
	$('#receiveBtn').click(function (){
		receiveKey($('#keyInput').val());
	});
	$('#keyInput').keyup(function (e) {
		if(e.keyCode == 13){
			$('#receiveBtn').click();
		}
	});
	$('#keyInput').keydown(function () {
		$('#receiveForm .form-group').removeClass('has-error');
		$('#status').text('').removeClass('text-danger').addClass('text-success');
	});
	updateDevice();
})   


